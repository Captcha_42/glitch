﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Glitch_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Jou, Sander");
            int arv = 7; // muutja defineerimine ja väärtustamine
            int teinearv = 0; // muutuja defineerimine (ilma väärtustamata) - halb komme
            teinearv = arv * 10; // muutja väärtustamine avaldisega
            Console.WriteLine(teinearv); // muutja kasutamine - väärtuse väljastamine

            // andmetüüp - fundamentaalmõiste
            //int kolmasarv = 5;
            //int = 100 // andmetüüp - täisarv, ratsionaalarv
            //Console.WriteLine((arv = kolmasarv) * 7);
            int kolmasarv = 0;
            kolmasarv += (arv += 7) * 2;
            Console.WriteLine(kolmasarv);
            arv = 20;
            Console.WriteLine(arv != 5);
            
        }
    }
}
